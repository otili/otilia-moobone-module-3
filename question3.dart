Expanded(
  child: GridView(
    padding: EdgeInsets.zero,
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      childAspectRatio: 1,
    ),
    scrollDirection: Axis.vertical,
    children: [
      FFButtonWidget(
        onPressed: () {
          print('Button pressed ...');
        },
        text: 'Feature1',
        options: FFButtonOptions(
          width: 130,
          height: 40,
          color: FlutterFlowTheme.of(context).primaryColor,
          textStyle: FlutterFlowTheme.of(context).subtitle2.override(
                fontFamily: 'Poppins',
                color: Colors.white,
              ),
          borderSide: BorderSide(
            color: Colors.transparent,
            width: 1,
          ),
          borderRadius: 12,
        ),
      ),
      FFButtonWidget(
        onPressed: () {
          print('Button pressed ...');
        },
        text: 'Feature2',
        options: FFButtonOptions(
          width: 130,
          height: 40,
          color: FlutterFlowTheme.of(context).primaryColor,
          textStyle: FlutterFlowTheme.of(context).subtitle2.override(
                fontFamily: 'Poppins',
                color: Colors.white,
              ),
          borderSide: BorderSide(
            color: Colors.transparent,
            width: 1,
          ),
          borderRadius: 12,
        ),
      ),
      FFButtonWidget(
        onPressed: () {
          print('Button pressed ...');
        },
        text: 'user profile edit',
        options: FFButtonOptions(
          width: 130,
          height: 40,
          color: FlutterFlowTheme.of(context).primaryColor,
          textStyle: FlutterFlowTheme.of(context).subtitle2.override(
                fontFamily: 'Poppins',
                color: Colors.white,
              ),
          borderSide: BorderSide(
            color: Colors.transparent,
            width: 1,
          ),
          borderRadius: 12,
        ),
      ),
      FFButtonWidget(
        onPressed: () {
          print('Button pressed ...');
        },
        text: 'Sign out',
        options: FFButtonOptions(
          width: 130,
          height: 40,
          color: FlutterFlowTheme.of(context).primaryColor,
          textStyle: FlutterFlowTheme.of(context).subtitle2.override(
                fontFamily: 'Poppins',
                color: Colors.white,
              ),
          borderSide: BorderSide(
            color: Colors.transparent,
            width: 1,
          ),
          borderRadius: 12,
        ),
      ),
    ],
  ),
)

